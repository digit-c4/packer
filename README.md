[![N|Solid](https://th.bing.com/th/id/R.c96fa31b891a2a8ebdd84b83b00f45e0?rik=lEvoSuW5PHEynQ&riu=http%3a%2f%2fwww.slexn.com%2fwp-content%2fuploads%2f2019%2f11%2fpacker.png&ehk=NqfN253rK%2f0x4XopmuNf%2fGaf5XSNWKOu1mlVUn2ko7o%3d&risl=&pid=ImgRaw&r=0)](https://www.packer.io/)
# Image / Template creation

## What is Packer
Hashicorp Packer is an open-source tool for creating Golden Machine Images for multiple cloud providers such as OVH, AWS and Machine Templates for VMware. Hashicorp Packer is able to perform configurations and install software on the image/template.

### What are Golden Images
A Golden Image is a pre-configured, customized template of a virtual machine. The aim of Golden Images is to enable you to create new virtual machines quickly, without having to install the basic tools or perform configuration every time you create a new virtual machine.
#### Benefits of Golden Images

- fast vm creation
- less configuration and installations
- less errors

## How does Packer Works
Packer creates a virtual machine on the cloud provider using an already existing image for OVH and AWS and then Packer will establish a connection to this new virtual machine and do the provisioning where it configures the virtual machine and does the installations of the tools provided.

Provisioning can be performed by a large number of different provisioners, but in our project we're using Ansible.

For VMware, it's a little different. First, Packer will download an ISO that we need to provide to the VMware server and then Packer will create a new VM with the downloaded ISO. To perform the configuration, we need a preseed file. A preseed file is a file in which all configurations are already defined, so that installation of the operating system can be performed without any human interaction. Once the virtual machine has been configured, it connects to the newly created virtual machine and performs provisioning. It then transforms the virtual machine into a template and saves it on the VMware server.

### Packer Project

The goal of this project is to create new virtual machine images/templates for the various cloud providers. With this project, not only will the images be created, but Packer will install all the tools we provide and it will also do all the configuration we want on this image. When the images/templates are created successfully, a new virtual machine will be created on which tests will be run to see if everything is installed and configured correctly. If this is the case, the virtual machine will be deleted, but if the tests have failed, the image/template and the test virtual machine will be deleted.
All these steps are grouped together in an Ansible AWX workflow.

![STAR.tech image](./docs/img/Debian12packer.drawio.png)

# The Project
## INSTALATION OF THE TOOLS NEEDED !!!
Before you can use this project, you need to run the tools.yaml playbook on the virtual machine that will later run the scripts.
This playbook will install all the necessary tools, such as Git, Ansible and Packer itself.

## Where can I see a list of tools installed and how to add more?

To see the list of installed tools, you need to open the image creation role. In the files folder you can see a file called "apt". If you open this file you can see the list of tools that will be installed on the image/template with apt.

![STAR.tech image](./docs/img/apt.png)

To add new tools, simply enter a new line with the apt tool name.

## Is docker going to be installed ?

Yes, Docker will be installed in its own role. For Docker, we had to do more configuration, so we created our own role. The role is located in image-roles/roles/docker.
In this role, everything I need for Docker will be installed.

- aptitude
- apt-transport-https
- ca-certificates
- software-properties-common
- virtualenv
- python3-setuptools

This role will add the Docker GPG apt key, download the latest stable version of the Docker repository and then install docker-ce and docker-compose. To be able to add the repository, you need to define a variable with the code name of the debian distribution, otherwise you'll get an error.

After installing docker and docker compose, the role will copy some configurations, add the user snet to the docker group and create some folders.

## How to create new Image / Template?

To create a new image/template, you need to go to Ansible AWX. There's a workflow called "Packer_workflow_vmware" - click on it.
![STAR.tech image](./docs/img/workflow.png)

You will now see some variables you need to define.
They are all necessary for the execution of the workflow.
( can be replaced later by vault )

## Differences of JRC

There is a difference in the scripts if you want to create a template with Packer on the JRC server.

![STAR.tech image](./docs/img/compatibility.png)

The Ansible provisioner for the JRC packer script looks like this.
It tells Packer to use the ssh-rsa algorythm and in the extra vars it tells Packer to run in compatibility mode.

Without this change for JRC, Packer cannot connect to the virtual machine it created, so there are 2 Packer scripts and you have to specify the variable "JRC: true" in the extra vars of ansible AWX when using on JRC.

### Problems with JRC

There were also some other problems with the worklow on the JRC server.
When the template was created and then a new virtual machine was started with this template. The virtual machine is not accessible via ssh. This is because the switch is not aware of this virtual machine. The solution to this problem was to add a small script in the if-up.d folder that does an arping to the gateway.
When the virtual machine starts, a arping to the gateway is automatically done and so the switch knows the virtual machine and the ssh connection can be established successfully.

![STAR.tech image](./docs/img/arping.png)

There was also a problem with the resolved script in this folder, so this script also needed to be changed.

![STAR.tech image](./docs/img/send-arping.png)

### Basic Settings:
Variable | Description
---|---
r_user | User to connect Packer vm
r_user | User to connect test vm
vm_name | name of the Template / Image
vm_name_new | name of the test vm 

### Cloud variable is needed for Workflow to work
Variable | Description
---|---
Cloud | which cloud is used (vmware, ovh, aws)
JRC | true or false (for JRC a compatible mode is needed so that Packer can establish an SSH connection to the new VM)

### Debian versions 11 = bullseye 12 = bookworm ( enter codename )
Variable | Description
---|---
debian_version | codename of debian distribution

### Network settings:
Variable | Description
---|---
Packer_ip | default IP for the Template
Packer_netmask | default netmask for the Template
Packer_gateway | default gateway for the Template
Packer_nameserver | default nameserver for the Template
dns_domains | dns domains for new vm
http_proxy | default http proxy for the Template
http_proxy | default https proxy for the Template

### VMware Settings:
Variable | Description
---|---
vmware_server | vmware host
vmware_username | vmware username
vmware_password | vmware password
vmware_server | vmware server
vmware_network | vmware network
vmware_datacenter | vmware datacenter
vmware_datastore | vmware datastore
vmware_cluster | vmware cluster

### Redmine host
Variable | Description
---|---
redmine_host | needed for git

Once you've defined all these variables, you can click on " Launch " and the rest will happen automatically.